# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - |10H|
| jak se mi to podařilo rozplánovat | Myslím si, že by to příště šlo rozplánovat lépe.|
| design zapojení | ** |
| proč jsem zvolil tento design | Prišel mi hodne gůd. |
| zapojení | ** |
| z jakých součástí se zapojení skládá | LED pásek, LCD displej, fotorezistor, LED zelená/červená, wemos, teploměr a shield |
| realizace | ** |
| nápad, v jakém produktu vše propojit dohromady| Nechápu :( |
| co se mi povedlo | Myslím si, že se mi povedlo snad vše až na funkčnost na konci.|
| co se mi nepovedlo/příště bych udělal/a jinak |Asi bych víc věřil v boha. |
| zhodnocení celé tvorby | Tvorba byla zdlouhavá a potom co jsem to vše udělal a fungovalo to, tak to přestane random fungovat. |
