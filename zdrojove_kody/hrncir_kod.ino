#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_NeoPixel.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

int photoPin = A0;
const int oneWireBus = 4;
int PIN = 6; //D4 / D6
#define STRIPSIZE 8
const byte SEMAFOR[] = {9,8,7}; //D7,D6,D5 / 9,8,7


//teplomer
OneWire oneWire(oneWireBus);
DallasTemperature sensors(&oneWire);

//pasek
Adafruit_NeoPixel strip = Adafruit_NeoPixel(STRIPSIZE, PIN, NEO_GRB + NEO_KHZ800);

//display
LiquidCrystal_I2C lcd(0x27,16,2);

void setup() {
  Serial.begin(115200);

  sensors.begin();

  strip.begin();
  strip.setBrightness(5);  // Lower brightness and save balls!
  strip.show();

  pinMode(SEMAFOR[0], OUTPUT);
  pinMode(SEMAFOR[1], OUTPUT);
  pinMode(SEMAFOR[2], OUTPUT);

  lcd.init();
  lcd.backlight();
}

/********************************************************************/
void photo_demo(){
  Serial.println(analogRead(photoPin)); 
  delay(500);
}



///////////
void teplomer_demo(){
  sensors.requestTemperatures(); 
  float temperatureC = sensors.getTempCByIndex(0);
  Serial.print(temperatureC + 105);
  Serial.println("ºC");
  delay(5000);
}




// Funkce Wheel
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void demo_rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
    }
    strip.show();
    delay(wait);
  }
}


///////////
 void demo_semafor(){
    for(byte i = 0; i < 3 ; i++){
      digitalWrite(SEMAFOR[i],HIGH);
      delay(200);
      digitalWrite(SEMAFOR[i],LOW);
      delay(200);
      }

}



void lcd_demo(){
 lcd.setCursor(0,0);
 lcd.print("Hautcododat");
 lcd.setCursor(0,1);
 lcd.print("Uzzasedelamtuhleblbost");


}
/********************************************************************/

void loop() {
 lcd_demo();
 demo_semafor();
 teplomer_demo();
 demo_rainbowCycle(5);
 photo_demo();
}

